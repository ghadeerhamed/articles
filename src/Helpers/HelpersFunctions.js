
const CryptoJS = require("crypto-js");
const CIPHER_KEY = 'secret-key';

export const ACTION_ADD = 'add';
export const ACTION_DELETE = 'delete';

export function existInStorage(key) {
    return localStorage.getItem(key) || false;
}

export function checkAuth() {
    return existInStorage('token');
}

export function saveToken(token) {
    localStorage.setItem('token', token);
}

export function logout() {
    localStorage.removeItem('token');
    return !existInStorage('token');
}

export function encrypt(value) {
    return CryptoJS.AES.encrypt(value, CIPHER_KEY).toString();
}

export function decrypt(value) {
    return CryptoJS.AES.decrypt(value, CIPHER_KEY).toString(CryptoJS.enc.Utf8);
}

export function getUsername(callback) {
    let user =  localStorage.getItem('user') || null;
    if (user) {
        user = JSON.parse(user);
        user = user.first_name + ' ' + user.last_name;
        callback(user);
    }
    return false;
}

export const asyncLocalStorage = {
    setItem: async function (key, value) {
        await null;
        await localStorage.setItem(key, value);
    },
    getItem: async function (key) {
        await null;
        return localStorage.getItem(key);
    }
};

export function getBase64(file, callback) {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        callback(reader.result)
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };
}