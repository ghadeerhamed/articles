import React, {Component} from 'react';
import 'babel-polyfill';
import './App.css';
import Header from "./components/Header";
import RouterManager from "./components/RouterManager";
import {getUsername} from "./Helpers/HelpersFunctions";
require('./components/indexdb');

class App extends Component {

    constructor(props){
        super(props);
        this.state = {logged: false, user: null}
    }

    componentDidMount() {
        const s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = false;
        s.innerHTML = `let db;
                        let dbReq = indexedDB.open('ArticlesDB', 1);
                        
                        dbReq.onupgradeneeded = function(event) {
                            db = event.target.result;
                            db.createObjectStore('articles', {autoIncrement: true});
                        };
                        
                        dbReq.onsuccess = function(event) {
                            db = event.target.result;
                        };
                        dbReq.onerror = function(event) {
                            alert('error opening database ' + event.target.errorCode);
                        };
                        `;

        document.body.appendChild(s);

        getUsername((user) => user && this.setState({logged: true, user: user}))
    }

    render() {
        return (
            <div className="App">
                <Header user={this.state.user}/>
                <RouterManager/>
            </div>
        );
    }
}


export default App;
