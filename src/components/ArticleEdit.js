import React, {Component} from "react";
import {Editor} from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from 'html-to-draftjs';
import {convertToRaw, EditorState, ContentState} from "draft-js";
import {getBase64} from "../Helpers/HelpersFunctions";
import {editArticle, getByKey} from "./indexdb";

export default class ArticleEdit extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: '',
            content: '',
            date: '',
            image: '',
        }
    }

    componentDidMount() {
        let key = this.props.match.params.key;
        key = parseInt(key);
        getByKey(key, ({title, content, date, image}) => {
            const blocksFromHtml = htmlToDraft(content);
            const {contentBlocks, entityMap} = blocksFromHtml;
            const contentState = ContentState.createFromBlockArray(contentBlocks, entityMap);
            const editorState = EditorState.createWithContent(contentState);

            this.setState({
                title: title,
                editorState: editorState,
                content: content,
                date: date,
                image: image,
            })
        })
    }


    handleChange = (evt) => {
        this.setState({[evt.target.id]: evt.target.value});
    };

    onEditorStateChange = (event) => {
        let editorState = event;
        let editorSourceHTML = draftToHtml(convertToRaw(event.getCurrentContent()));
        this.setState({
            editorState: editorState,
            content: editorSourceHTML
        });
    };

    handleChangeImage = (event) => {
        if (event.target.files[0] && event.target.files[0].size / 1024 / 1024 > 4)
            alert("Max image size is 4 MB. Choose another Image");
        else
            getBase64(event.target.files[0], (file) => {
                this.setState({
                    image: file
                })
            });
    };

    handleSubmit = (e) => {
        this.editArticle(e);
    };

    editArticle = (e) => {
        e.preventDefault();
        let key = this.props.match.params.key;
        key = parseInt(key);
        const data = {
            title: this.state.title,
            content: this.state.content,
            date: this.state.date,
            image: this.state.image,
        };
        editArticle(key, data);
        this.props.history.push('/')
    };

    render() {
        const {title, editorState, date} = this.state;
        return (
            <div>
                <h2>Edit Article</h2>

                <form className="form-control" onSubmit={this.handleSubmit}>
                    <div>
                        <label className="form-label" htmlFor='title'>Title</label>
                        <input
                            className="form-control"
                            type='text'
                            id='title'
                            value={title}
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                    <div>
                        <label className="form-label" htmlFor='editorState'>Content</label>
                        <Editor
                            id='editorState'
                            editorState={editorState}
                            toolbarClassName="toolbar"
                            wrapperClassName="wrapper"
                            editorClassName="editor"
                            onEditorStateChange={this.onEditorStateChange}
                        />
                    </div>
                    <div>
                        <label className="form-label" htmlFor='date'>Date</label>
                        <input
                            className="form-control"
                            type='date'
                            id='date'
                            value={date}
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                    <div>
                        <label className="form-label" htmlFor='image'>Image</label>
                        <input
                            className="form-control"
                            type='file'
                            id='image'
                            onChange={this.handleChangeImage}
                        />
                    </div>
                    <img src={this.state.image} className="image-preview"/>
                    <div>
                        <button className="form-submit">Submit</button>
                    </div>
                </form>
            </div>
        )
    }
}