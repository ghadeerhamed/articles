import React, {Component} from "react";
import {deleteArticle} from "./indexdb";
import ArticleEdit from "./ArticleEdit";
import {Link, Redirect} from "react-router-dom";

export default class ArticleBox extends Component {

    constructor(props) {
        super(props);
    }

    deleteArticle = () => {
        const {key} = this.props.article;
        deleteArticle(key, () => {
            this.props.deleted(key)
        })
    };

    render() {
        const article = this.props.article;
        const {title, content, date, image, key} = article;

        return (
            <div className="article-box">
                <h2>{title}</h2>
                <div dangerouslySetInnerHTML={{__html: content}}/>
                <h5>{date}</h5>
                <img className="image-preview" src={image}/>
                <div className="btns-group">
                    <button onClick={this.deleteArticle} className="form-submit btn-delete"> Delete</button>
                    <Link to={`/article/edit/${key}`}>
                        <button className="form-submit btn-edit">
                            Edit
                        </button>
                    </Link>
                </div>
            </div>
        )
    }
}