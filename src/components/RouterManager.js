import React, {Component} from "react";
import {Route, Redirect} from "react-router-dom";
import Login from './Login'
import ArticleList from "./ArticleList";
import Register from "./Register";
import {checkAuth, logout} from "../Helpers/HelpersFunctions";
import ArticleCreate from "./ArticleCreate";
import ArticleEdit from "./ArticleEdit";

export default class RouterManager extends Component {
    render() {
        return (
            <div>
                <Route exact path='/' render={() => (checkAuth() ? <ArticleList/> : <Redirect to='/login'/>)}/>
                <Route exact path='/login' render={(routeProps) => (!checkAuth() ? <Login {...routeProps}/> : <Redirect to='/'/>)}/>
                <Route exact path='/register' render={(routeProps) => (!checkAuth() ? <Register {...routeProps}/> : <Redirect to='/'/>)}/>
                <Route exact path='/logout' render={() => (logout() ? <Redirect to='/login'/> : <Redirect to='/'/>)}/>
                <Route exact path='/article/create' render={() => (checkAuth() ? <ArticleCreate/> : <Redirect to='/login'/>)}/>
                <Route exact path='/article/edit/:key' render={(routeProps) => (checkAuth() ? <ArticleEdit {...routeProps}/> : <Redirect to='/login'/>)}/>
            </div>
        );
    }
}