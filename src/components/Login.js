import React, {Component} from "react";
import {checkAuth, decrypt } from "../Helpers/HelpersFunctions";
import {Link} from "react-router-dom";

const initialState = {username: "", password: "", wrong: false};

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = initialState;
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(evt) {
        this.setState({[evt.target.id]: evt.target.value});
    }


    handleSubmit(evt) {
        evt.preventDefault();
        const {username, password} = this.state;
        let users = JSON.parse(localStorage.getItem('users') || '[]');
        const user = users.find(user => user.username === username && decrypt(user.password) === password);
        if (user) {
            localStorage.setItem('token', username);
            localStorage.setItem('user', JSON.stringify(user));
            this.setState(initialState);
        } else this.setState({wrong: true})
    }

    render() {
        return (
            <div>
                {checkAuth() ? this.props.history.push('/') : ''}
                {this.state.wrong ? <h4>Username Or Password Not correct.. Try again.</h4> : ''}
                <h3> Login </h3>
                <form className="form-control" onSubmit={this.handleSubmit}>
                    <div>
                        <label className="form-label" htmlFor='username'>Username</label>
                        <input
                            className="form-control"
                            type='text'
                            id='username'
                            value={this.state.username}
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                    <div>
                        <label className="form-label" htmlFor='password'>Password</label>
                        <input
                            className="form-control"
                            type='password'
                            id='password'
                            value={this.state.password}
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                    <div>
                        <button className="form-submit">Submit!</button>
                    </div>
                    <div>
                        Or create <Link to="/register">new account </Link>
                    </div>
                </form>
            </div>
        )
    }
}

export default Login;