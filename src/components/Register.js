import React, {Component} from "react";
import './../App.css'
import {checkAuth, encrypt} from "../Helpers/HelpersFunctions";
import {Link, Redirect} from "react-router-dom";

const initialState = {first_name: "", last_name: "", username: "", password: "", exist: false};

export default class Register extends Component {

    constructor(props) {
        super(props);
        this.state = initialState;
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        // saveToken("token123");
    }


    handleChange(evt) {
        this.setState({[evt.target.id]: evt.target.value});
    }

    handleSubmit(evt) {
        evt.preventDefault();
        const {username, first_name, last_name, password} = this.state;
        let users = JSON.parse(localStorage.getItem('users') || '[]');
        const exist = users.some(user => user.username === username);
        if (!exist) {
            const passwordEnc = encrypt(password);
            const newUser = {username: username, first_name: first_name, last_name: last_name, password: passwordEnc};
            users.push(newUser);
            localStorage.setItem('users', JSON.stringify(users));
            localStorage.setItem('user', JSON.stringify(newUser));
            localStorage.setItem('token', username);
            this.setState(initialState);
        } else this.setState({exist: true})

    }

    render() {
        return (
            <div>
                {checkAuth() ? <Redirect to='/'/> : ''}
                {this.state.exist ? <h4>Username already taken.. Try again.</h4> : ''}
                <h3>Create new account</h3>
                <form className="form-control" onSubmit={this.handleSubmit}>
                    <div>
                        <label className="form-label" htmlFor='first_name'>First Name</label>
                        <input
                            className="form-control"
                            type='text'
                            id='first_name'
                            value={this.state.first_name}
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                    <div>
                        <label className="form-label" htmlFor='last_name'>Last Name</label>
                        <input
                            className="form-control"
                            type='text'
                            id='last_name'
                            value={this.state.last_name}
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                    <div>
                        <label className="form-label" htmlFor='username'>Username</label>
                        <input
                            className="form-control"
                            type='text'
                            id='username'
                            value={this.state.username}
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                    <div>
                        <label className="form-label" htmlFor='password'>Password</label>
                        <input
                            className="form-control"
                            type='password'
                            id='password'
                            value={this.state.password}
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                    <div>
                        <button className="form-submit">Submit</button>
                    </div>
                    <div>
                        Or <Link to="/login">login </Link>
                    </div>
                </form>
            </div>
        )
    }
}