import React from "react";
import {checkAuth} from "../Helpers/HelpersFunctions";
import {Link} from "react-router-dom";

export default class Header extends React.Component {

    render() {
        return (
            <nav className="navbar navbar-brand navbar-expand-lg navbar-dark" >
                {checkAuth() ? (<span style={{backgroundColor: "#eee"}}><a href='/logout' > Logout</a>  | {this.props.user} </span> ) : ''}
                <a className="navbar-brand" href='/'>Articles </a>
                <Link to='/article/create' className="nav-link nav-item" >Create New Article </Link>
            </nav>
        )
    }
}