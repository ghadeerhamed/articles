import React, {Component} from "react";
import {addArticle} from "./indexdb";
import {EditorState, convertToRaw} from 'draft-js';
import {Editor} from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import {getBase64} from "../Helpers/HelpersFunctions";

const initialState = {title: "", editorState: EditorState.createEmpty(), content: "", date: "", image: ""};
export default class ArticleCreate extends Component {

    constructor(props) {
        super(props);
        this.state = initialState;
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        // addArticle({test: "Test"});
    }

    handleChange(evt) {
        this.setState({[evt.target.id]: evt.target.value});
    }

    onEditorStateChange = (event) => {
        let editorState = event;
        let editorSourceHTML = draftToHtml(convertToRaw(event.getCurrentContent()));

        this.setState({
            editorState: editorState,
            content: editorSourceHTML
        });
    };

    handleChangeImage = (event) => {
        if (event.target.files[0] && event.target.files[0].size / 1024 / 1024 > 4)
            alert("Max image size is 4 MB. Choose another Image");
        else
            getBase64(event.target.files[0], (file) => {
                this.setState({
                    image: file
                })
            });
    };

    handleSubmit = (e) => {
        this.addArticle(e);
    };

    addArticle = (e) => {
        e.preventDefault();
        addArticle(this.state);
        this.setState(initialState)
    };

    render() {
        const {title, editorState, date} = this.state;
        return (
            <div>
                <h2>Create</h2>

                <form className="form-control" onSubmit={this.handleSubmit}>
                    <div>
                        <label className="form-label" htmlFor='title'>Title</label>
                        <input
                            className="form-control"
                            type='text'
                            id='title'
                            value={title}
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                    <div>
                        <label className="form-label" htmlFor='editorState'>Content</label>
                        <Editor
                            id='editorState'
                            editorState={editorState}
                            toolbarClassName="toolbar"
                            wrapperClassName="wrapper"
                            editorClassName="editor"
                            onEditorStateChange={this.onEditorStateChange}
                        />
                    </div>
                    <div>
                        <label className="form-label" htmlFor='date'>Date</label>
                        <input
                            className="form-control"
                            type='date'
                            id='date'
                            value={date}
                            onChange={this.handleChange}
                            required
                        />
                    </div>
                    <div>
                        <label className="form-label" htmlFor='image'>Image</label>
                        <input
                            className="form-control"
                            type='file'
                            id='image'
                            onChange={this.handleChangeImage}
                            required
                        />
                    </div>
                    <img src={this.state.image} className="image-preview"/>
                    <div>
                        <button className="form-submit">Submit!</button>
                    </div>
                </form>
            </div>
        )
    }
}