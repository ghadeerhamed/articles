let db;
let dbReq = indexedDB.open('ArticlesDB', 1);

dbReq.onupgradeneeded = function (event) {
    db = event.target.result;
    db.createObjectStore('articles', {autoIncrement: true});
};

dbReq.onsuccess = function (event) {
    db = event.target.result;
};
dbReq.onerror = function (event) {
    alert('error opening database ' + event.target.errorCode);
};

export function addArticle(article) {
    let tx = db.transaction(['articles'], 'readwrite');
    let store = tx.objectStore('articles');

    let articleObj = {title: article.title, content: article.content, date: article.date, image: article.image};
    store.add(articleObj);

    tx.oncomplete = function () {
        // console.log('stored article!', articleObj);
        return articleObj
    };
    tx.onerror = function (event) {
        alert('error storing article ' + event.target.errorCode);
    }
}

export function getByKey(key, callback) {

    let tx = db.transaction(['articles'], 'readonly');
    let store = tx.objectStore('articles');

    let req = store.get(key);

    req.onsuccess = function (event) {
        let article = event.target.result;
        if (article) {
            callback(article)
        } else {
            console.log(`article ${key} not found`)
        }
    };

    req.onerror = function (event) {
        alert('error getting article 1 ' + event.target.errorCode);
    }
}

export function getArticles(callback) {
    let tx = db.transaction(['articles'], 'readonly');
    let store = tx.objectStore('articles');

    let req = store.openCursor();
    let allArticles = [];

    req.onsuccess = function (event) {
        let cursor = event.target.result;
        if (cursor != null) {
            const k = cursor.key;
            const val = cursor.value;
            val.key = k;
            allArticles.push(val);
            cursor.continue();
        } else {
            callback(allArticles);
        }
    };

    req.onerror = function (event) {
        alert('error in cursor request ' + event.target.errorCode);
    }
}

export function deleteArticle(key, callback) {

    let tx = db.transaction(['articles'], 'readwrite');
    let store = tx.objectStore('articles');

    let req = store.delete(key);

    req.onsuccess = function (event) {
        callback()
    };

    req.onerror = function (event) {
        alert('error getting article 1 ' + event.target.errorCode);
    }
}

export function editArticle(key, article) {
    let tx = db.transaction(['articles'], 'readwrite');
    let store = tx.objectStore('articles');

    let articleObj = {title: article.title, content: article.content, date: article.date, image: article.image};
    let req = store.put(articleObj, key);

    req.onsuccess = function (event) {
        let article = event.target.result;
        if (article) {
            console.log(article)
        } else {
            console.log(`article ${key} not found`)
        }
    };

    req.onerror = function (event) {
        alert('error getting article 1 ' + event.target.errorCode);
    }
}