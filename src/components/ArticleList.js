import React, {Component} from "react";
import {getArticles} from "./indexdb";
import ArticleBox from "./ArticleBox";

export default class ArticleList extends Component {

    constructor(props) {
        super(props);
        this.state = {articles: [], loading: true}
    }

    articleDeleted = (key) => {
        this.setState({articles: this.state.articles.filter(ar => ar.key !== key)})
    };

    componentDidMount() {
        setTimeout(function () {
            getArticles(articles => {
                this.setState({articles: articles, loading: false})
            });
        }.bind(this), 1000);
    }

    render() {
        const {articles, loading} = this.state;
        return (
            <div>
                <h2>Home Page Articles</h2>
                {
                    loading ? <h4> Loading .... </h4> : articles.map((article) => <ArticleBox key={article.key}
                                                                                              article={article}
                                                                                              deleted={this.articleDeleted
                                                                                              }/>)
                }
            </div>
        )
    }
}